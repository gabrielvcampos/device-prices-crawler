# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import pymssql


class ZoomPipeline(object):
    def process_item(self, item, spider):
        return item


class SqlServerPipeline(object):
    def __init__(self):
        self.conn = pymssql.connect(host='XXXXXXXX', user='XXXXXXX', password='XXX!XX', database='XXXx')
        self.cursor = self.conn.cursor()
        #log data to json file


    def process_item(self, item, spider): 

        try:
            self.cursor.execute("INSERT INTO API.ZoomCrawler(url, device_name, price) VALUES (%s, %s, %s)", (item['url'], item['device_name'], item['price']))
            self.conn.commit()

        except pymssql.Error as e:
            print ("error")

            return item