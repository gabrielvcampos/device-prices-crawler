# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class ZoomSpiderSpider(CrawlSpider):
    name = 'zoom_spider'
    allowed_domains = ['zoom.com.br']
    start_urls = ['https://www.zoom.com.br/']
    user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"
    rules = (
        # Rule(LinkExtractor(allow="celular/smartphone")), 
        Rule(LinkExtractor(allow="celular/smartphone"), callback="parse_smartphone", follow=True),
    )
    
    def parse_smartphone(self, response):
        device_name = response.xpath('//*[@id="productInfo"]/h1/span/text()').extract_first()
        price = response.xpath('//*[@id="productInfo"]/div/div[2]/p/span[2]/a/strong/text()').extract_first()
        url = response.request.url
        final = {
            'device_name': device_name,
            'price': price,
            'url': url
        }
        yield final